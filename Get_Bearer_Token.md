# ClickUp Bearer Token

To get the bearer token from your request/response headers that are sent between ClickUp and your browser, open the ClickUp web app in the browser of your choice. Screenshots are taken using MS Edge, however, it should be similiar with other popular browsers.
After you have logged in, press `F12` to open the built in *development tools*. You should see something similiar, like on the following screenshot.

Make sure you are on the tab *Network* (1). If no traffic is shown, try to refresh the ClickUp page. Maybe you even have to press the button record (next to button *Delete* (2) in the picture). As soon as traffic is coming up, it is rapidly filling the screen. Click on the button *Delete* (2) to clear the screen.

![](get_bearer01.png)

Next step is to trigger an action. Just create a new task.

![](get_bearer02.png)

You will see a corresponding request and response object on the tools screen, as marked in the screenshot.

![](get_bearer03.png)

Now click on the response object and have a look at the response headers. Please make sure that you are on the *Header* tab (1). Scroll down until you find the response header (2) where you should find the `authorization` (3) entry with your bearer token.

![](get_bearer04.png)