# Create ClickUp Project

Windows executable to create project lists in ClickUp with predefined views and settings.

For usage help just run ```create_clickup_project.exe -h```.
Or just execute the script and insert the required information.